// Requete 1 - Compter le nombre de films deplus de 3 heures.
db.movies.count(
    { duration: {$gt: 180} }
)

// Requete 2 - Lister les films sortis après 2015(inclus). Calculer également leur nombre.
db.movies.find(
    { title_year: {$gte: 2015}},
    { movie_title: true, title_year: true, _id:false}
)

db.movies.count(
    { title_year: {$gte: 2015}}
)

// Requete 3 - Lister les films des années 80, triés par année. N’afficher que le titre du film et son année
db.movies.find(
    { title_year: {$gte: 1980 , $lt: 1990}},
    { movie_title: true, title_year: true, _id:false}
).sort(
    {
        title_year: 1
    }
)

db.movies.count(
    { title_year: {$gte: 1980 , $lt: 1990}}
)

// Requete 4 - Lister les films inscrits dans le genre "Science-Fiction".
db.movies.find(
    { genres: "Sci-Fi"},
    { movie_title: true, genres: true, _id:false}
)

db.movies.count(
    { genres: "Sci-Fi"}
)

// Requete 5 - Lister les films dont au moins un acteur s’appelle "Stone".
db.movies.find(
    { "actors.name": /Stone/i},
    { movie_title: true, actors: true, _id:false}
)

db.movies.count(
    { "actors.name": /Stone/i}
)

// Requete 6 - Lister les films sortis depuis 2015, et dont la note IMDB est supérieure ou égale à 8
db.movies.find(
    { title_year: {$gte: 2015}, imdb_score: {$gte: 8}},
    { movie_title: true, imdb_score: true, title_year: true,_id:false}
)

db.movies.count(
    { title_year: {$gte: 2015}, imdb_score: {$gte: 8}}
)

// Requete 7 - Lister les 10 films ayant eu les plus grandes recettes brutes, triés par ordre décroissant, 
// en n’affichant que leur titre et leurs recettes brutes.
db.movies.find(
    { gross: {$ne: ""}},
    { movie_title: true, gross: true, _id:false}
).limit(10).sort(
        {
            gross: -1
        }
)

// Requete 8 - Lister les films les plus rentables,du rang 11 au rang 20.
db.movies.find(
    {gross: {$ne: ""} , budget: {$ne: ""}},
    { movie_title: true, gross: true, budget: true,_id:false}
).limit(10).skip(10)

// Requete 9 - Lister  les  films  inscrits  à  la  fois  dans  les  genres  "Comédie",  "Thriller","Science-Fiction".
db.movies.find(
    {$and : [{genres: "Sci-Fi"}, {genres: "Comedy"}, {genres: "Thriller"} ] },
    { movie_title: true, genres: true, _id:false}
)
