// Requete 1 Listez les 10 films ayant eu les plus grandes recettes brutes, triés par ordre décroissant, 
//en n’affichant que leur titre et leurs recettes brutes.
// Question à traiter cette fois par le "Aggregation pipeline"
db.movies.aggregate([
    {$sort:
        {gross: -1}
    },
    {$limit: 10},
    {$project: 
        {movie_title: true, gross: true, _id: false}
    },
])

// Requete 2 - Affichez tous les titres de films en majuscules.(utilisez l’opérateur $toUpper dans votre projection)
db.movies.aggregate([
    {$project:
        {movie_title: { $toUpper: "$movie_title" }, _id:false}
    }
])

// Requete 3 - Calculez la marge de chaque film.(utilisez l’opérateur $subtract dans votre projection)
db.movies.aggregate([
    {$project: 
        {marge: {$subtract : ["$gross", "$budget"] }, movie_title: true, _id: false } 
    }
])

// Requete 4 - Déduisez-en la liste des films les plus rentables.
db.movies.aggregate([
    {$project: 
        {marge: {$subtract : ["$gross", "$budget"] }, movie_title: true, _id: false } 
    },
    {$sort:
        {marge: -1}
    },
])

// Requete 5 - Déduisez-en également lenombre de films non rentables

db.movies.aggregate([
    {$project: 
        {marge: {$subtract : ["$gross", "$budget"] },movie_title: true, _id: false } 
    },
    {$match:  { marge: {$lt: 0}}},
    {$count: "nbFilms"}
])


// Requete 6 - Comptez le nombre de films par pays
db.movies.aggregate([
    {$group: 
        {
            // GROUP BY country
            _id: "$country",

            total: {$sum: 1}
        }
    }
])

// Requete 7 -  Donnez le nombre de films par année
db.movies.aggregate([
    {$group: 
        {
            // GROUP BY title_year
            _id: "$title_year",

            total: {$sum: 1}
        }
    }
])

// Requete 8 -Comptez le nombre de films par année, en triant les résultats de façon décroissante.
db.movies.aggregate([
    {$group: 
        {
            // GROUP BY title_year
            _id: "$title_year",
            total: {$sum: 1}
        }
    },
    {$sort:
        {_id: -1}
    },
])

// Requete 9 -  Par extension, affichez uniquement l’année qui a produit le plus grand nombre de films.
db.movies.aggregate([
    {$group: 
        {
            // GROUP BY title_year
            _id: "$title_year",
            total: {$sum: 1}
        }
    },
    {$sort:
        {total: -1}
    },
    {$limit : 1}
])

// Requete 10 - Listez le nombre de films par Content Rating(et par nombre de films décroissant)
db.movies.aggregate([
    {$group: 
        {
            _id: "$content_rating",
            total: {$sum: 1}
        }
    },
    {$sort:
        {total: -1}
    }
])

// Requete 11 -  Donnez ensuite le nombre de films pour le Content Rating PG-13 uniquement
db.movies.aggregate([
    {$group: 
        {
            _id: "$content_rating",
            total: {$sum: 1}
        }
    },
    {$match:  { _id: "PG-13"}},
])


