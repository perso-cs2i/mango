// Requete 12 -  Listez les titres de films en double(ou plus ?)
db.movies.aggregate([
    {$group: 
        {
            _id: "$movie_title",
            total: {$sum: 1}
        }
    },
    {$sort:
        {total: -1}
    },
    {$match:  { total: {$gte: 2}}},
    //{$count: "total"}
])

// Requete 13 -  Calculez la durée totale(en minutes) des films, par année
db.movies.aggregate([
    {$match: {duration: {$ne: ""}}},
    {$group: 
        {
            _id: "$title_year",
            total: {$sum: "$duration"}
        }
    },
])


// Requete 14 - Quelle est l’année qui a rapporté le plus pour l’industrie du cinéma (champ gross uniquement) ?
db.movies.aggregate([
    {$match: {gross: {$ne: 0}}},
    {$group: 
        {
            _id: "$title_year",
            total: {$sum: "$gross"}
        }
    },
    {$sort:
        {total: -1}
    },
    {$limit: 1}
])

// Requete 15 - Affichez un bilan par année, pour les 10 années ayant rapporté le plus.Chaque année sera sous cette forme 
db.movies.aggregate([
    {$match: {gross: {$ne: 0}}},
    {$group: 
        {
            _id: "$title_year",

            total_gross: {$sum: "$gross"},
            average_gross: {$avg: "$gross"},
            min_gross: {$min: "$gross"},
            max_gross: {$max: "$gross"},
            movies: {$sum : 1}
        }
    },
    {$sort:
        {total_gross: -1}
    },
    {$project: 
        {total_gross: true, average_gross: true, min_gross:true, max_gross: true, movies: true } 
    },
    {$limit: 10}
]).pretty()

// Requete 16 - Listez les 10 réalisateurs ayant fait le plus de films.
db.movies.aggregate([
    {$match: {director_name: {$ne: ""}}},
    {$group: 
        {
            _id: "$director_name",
            movies: {$sum : 1}
        }
    },
    {$sort:
        {movies: -1}
    },
    {$project: 
        {director_name: true, movies: true } 
    },
    {$limit: 10}
])

// Requete 17 - Peter Jackson a tourné 12 films. Est-ce vrai ?
db.movies.aggregate([
    {$match: {director_name: /Peter Jackson/i}},
    {$group: 
        {
            _id: "$director_name",
            movies: {$sum : 1}
        }
    },
    {$project: 
        {director_name: true, movies: true } 
    }
])

// Requete 18 - Comptez les films par langue et par pays, par total décroissant.
db.movies.aggregate([
    {$group: 
        {
            _id: {language: "$language", country: "$country"},
            total: {$sum: 1}
        }
    },
    {$sort:
        {total: -1}
    }
])

// Requete 19 - Idem, en triant cette fois la liste par langue puis par total décroissant
db.movies.aggregate([
    {$match: {language: {$ne: ""}}},
    {$group: 
        {
            _id: {language: "$language"},
            total: {$sum: 1}
        }
    },
    {$sort:
        {total: -1}
    }
])

// Requete 20 - Idem, avec seulement les films en langue anglaise
db.movies.aggregate([
    {$match: {language: /English/i}},
    {$group: 
        {
            _id: "$language",
            total: {$sum: 1}
        }
    }
])