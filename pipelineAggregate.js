// pipeline d'aggrégation
// SKIP/LIMPIT avec le pipeline
db.movies.aggregate([
    {$match:
        {country: 'France', title_year: 2008}
    },
    {$skip: 100},
    {$limit: 7},
    {$project: 
        {movie_title: true, title_year: true, _id: false}
    },
    {$sort:
        {title_year: -1, movie_title: 1}
    }
])

// la même chose sans pipeline d'aggrégation
// "en mode classique"
db.movies.find().skip(100).limit(7)

db.movies.getIndexes()

db.movies.explain().find({country: 'France'})

db.movies.createIndex({country: 1})

db.movies.count({country: 'France'})

db.movies.explain().find({country: /Fra/i})

// exemple de GROUP BY
// SELECT title_year, COUNT(*) AS total
// FROM movies
// GROUP BY title_year;

db.movies.aggregate([
    {$group: 
        {
            // GROUP BY title_year
            _id: "$title_year",

            total: {$sum: 1}
        }
    }
])

db.movies.aggregate([
    {$group: 
        {
            // GROUP BY title_year
            _id: "$title_year",

            total: {$sum: "$gross"}
        }
    }
])
