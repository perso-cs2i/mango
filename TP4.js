// Requete 1 - Supprimez les films icelandais de la base de données.
db.movies.distinct("country")

db.movies.find(
    { country : "Iceland"},
    { country: true, movie_title: true, _id: false}
)

db.movies.deleteMany({ country : "Iceland" })

// Requete 2 - Corrigez les champs suivants :
// Lorsque  leurs  valeurs  sont  inconnues,  ces  champs  ont  actuellemen tpour valeur "" au lieu de 0.

// - title_year,
db.movies.find(
    { title_year : ""},
    { title_year: true, movie_title: true, _id: false}
)

db.movies.count({ title_year : ""})

db.movies.updateMany({title_year: ""}, {$set: {title_year: 0}})

db.movies.find(
    { title_year : 0},
    { title_year: true, movie_title: true, _id: false}
)

db.movies.count({ title_year : 0})

// - gross,
db.movies.find(
    { gross : ""},
    { gross: true, movie_title: true, _id: false}
)

db.movies.count({ gross : ""})

db.movies.updateMany({gross: ""}, {$set: {gross: 0}})

db.movies.find(
    { gross : 0},
    { gross: true, movie_title: true, _id: false}
)

db.movies.count({ gross : 0})

// - budget,
db.movies.find(
    { budget : ""},
    { budget: true, movie_title: true, _id: false}
)

db.movies.count({ budget : ""})

db.movies.updateMany({budget: ""}, {$set: {budget: 0}})

db.movies.find(
    { budget : 0},
    { budget: true, movie_title: true, _id: false}
)

db.movies.count({ budget : 0})

// - duration.
db.movies.find(
    { duration : ""},
    { duration: true, movie_title: true, _id: false}
)

db.movies.count({ duration : ""})

db.movies.updateMany({duration: ""}, {$set: {duration: 0}})

db.movies.find(
    { duration : 0},
    { duration: true, movie_title: true, _id: false}
)

db.movies.count({ duration : 0})

// D’autres champs secondaires ont également le problème...
/*color
num_critic_for_reviews
num_user_for_reviews
language
country
content_rating
aspect_ratio
*/

// Requete 3 -  Modifiez le titre du film Raiders of the Lost Ark pour qu’il soit en majuscules
db.movies.updateOne(
    // Query document : WHERE ?
    {movie_title: "Raiders of the Lost Ark"},

    //update document
    {
        $set:
        {
            movie_title: "RAIDERS OF THE LOST ARK"
        }
    }
)

db.movies.find(
    {movie_title: "Raiders of the Lost Ark"}
)

db.movies.find(
    {movie_title: "RAIDERS OF THE LOST ARK"},
    {genres: true, movie_title: true, _id: false}
)

// Requete 4 - Ajoutez le genre Comedy au même film
db.movies.updateOne(
    // Query document : WHERE ? sur qui ?
    {movie_title: "RAIDERS OF THE LOST ARK"},

    //update document
    {
        $set:
        {
            genres : ["Action", "Adventure","Comedy"]
        }

        /*$push:
        {
            genres: "Comedy"
        }*/
    }
)

db.movies.find(
    {movie_title: "RAIDERS OF THE LOST ARK"},
    {genres: true, movie_title: true, _id: false}
)

// Requete 5 -  Ajoutez un champ Récompenses —awards— à la collection Movies.
// Un film peut avoir de 0 à plusieurs récompenses, et la modification doit impacter tous les films.
db.movies.updateMany(
    {}, 
    {
        $set: {recompenses:[]}
    }
)

db.movies.find(
    {},
    {recompenses: true, movie_title: true, _id: false}
)

db.movies.count()

// Requete 6 - Supprimez le champ aspect_ratio de la collection de films.
db.movies.find(
    {},
    {movie_title: true, aspect_ratio: true, _id: false}
)

db.movies.updateMany(
    {},
    { 
        $unset:
        {
            aspect_ratio : true
        }
    }
)

db.movies.find(
    {},
    {movie_title: true, _id: false}
)

// Requete 7 - Ajoutez un acteur au film Star Wars, épisode IV
db.movies.find(
    {movie_title: /episode IV/i},
    {movie_title: true, actors: true, _id: false}
)

db.movies.updateOne(
    
    // Query document : WHERE ? sur qui ?
    {movie_title: /episode IV/i},

    //update document
    {
        $push:
        {
            actors: 
            [
                {
                    "name":"Céline Lescop",
                    "facebook_likes": 20000
                }
                
            ]
           
        }
    }
    
)

// Requete 8 - Supprimez ce même acteur.
db.movies.updateOne(
    
    // Query document : WHERE ? sur qui ?
    {movie_title: /episode IV/i},

    //update document
    {
        $pull:
        {
            actors: 
            [
                {
                    "name":"Céline Lescop",
                    "facebook_likes": 20000
                }
                
            ]
           
        }
    }
    
)

db.movies.find(
    {movie_title: /episode IV/i},
    {movie_title: true, actors: true, _id: false}
)

// Requete 9 - Écrivez la requête qui supprimerait tous les films de la base
db.movies.deleteMany(
    {}
)

