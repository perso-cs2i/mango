// Requete 1 - Lister tous les films
db.movies.find()

// Requete 2 - Lister tous les films de James Cameron.
db.movies.find(
    { director_name: "James Cameron" }
)

// Requete 3 - Lister tous les films, en n’affichant que leur titre et leur réalisateur.
db.movies.find(
    {},
    { movie_title: true, director_name: true } // SELECT
)

// Requete 4 - Lister tous les films de 2016
db.movies.find(
    { title_year: 2016 },
    { movie_title: true }

)

// Requete 5 -Calculer le nombre de films au total, 
db.movies.count()
//et le nombre de films pour 2016.
db.movies.count(
    { title_year: 2016 }
)
// Calculer par extension la part que représentent les films de 2016 sur l’ensemble des films.
let total = db.movies.count()
let annee = db.movies.count(
    { title_year: 2016 }
)
let part = annee / total * 100;
part

// Requete 6 -  Lister les films de la base de données, par ordre alphabétique
db.movies.find(
    {},
    { movie_title: true,_id:false}
).sort(
    {
        movie_title: 1
    }
)

// Requete 7 - Même question en limitant la liste à 10 films, et en n’affichant que le titre et l’année du film.
db.movies.find(
    {},
    { movie_title: true, title_year: true, _id:false}
).limit(10).sort(
    {
        movie_title: 1
    }
)

// Requete 8 - Lister puis compter les films anglais(2 requêtes distinctes).
db.movies.find(
    {language: "English"},
    { movie_title: true, language: true, _id: false}
)

db.movies.count(
    { language: "English"}
)

// Requete 9 - Lister les films dont le titre contient "star" donc LIKE '%star%'en SQL
db.movies.find(
    { movie_title: /star/i},
    { movie_title: true, _id: false}
)

// Requete 10 - Lister les films dont le titre démarre par "My" donc LIKE ’My%’en SQL
db.movies.find(
    { movie_title: /^My/i},
    { movie_title: true, _id: false}
)

// Requete 11 - Lister les films dont le titre se termine par "river".
db.movies.find(
    { movie_title: /river$/i},
    { movie_title: true, _id: false}
)

// Requete 12 - Compter le nombre de films dont le titre contient "America" (casse in-différente).
db.movies.count(
    { movie_title: /America/i}
)
