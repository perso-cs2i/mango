// Requete 1 - Trouver l’année du film le plus ancien, et celle du film le plus récent.(2 requêtes distinctes)
db.movies.find(
    { title_year: {$ne: ""}},
    { movie_title: true, title_year: true, _id:false}
).limit(1).sort(
    {
        title_year : 1
    }
)

db.movies.find(
    { title_year: {$ne: ""}},
    { movie_title: true, title_year: true, _id:false}
).limit(1).sort(
    {
        title_year : -1
    }
)

// Requete 2 - Quel est le film avec le moins de recettes?
db.movies.find(
    { gross: {$ne: ""}},
    { movie_title: true, gross: true, _id:false}
).limit(1).sort(
    {
        gross : 1
    }
)

// Requete 3 - Combien y a-t-il de films dont l’année est incorrecte ?
db.movies.count(
    { title_year: ""},
)

// Requete 4 - Dans  quels  genres  différents  sont  répartis  les  films  (genres  triés  par ordre alphabétique) ?
// Combien y a-t-il de genres ?
db.movies.distinct("genres")

db.movies.distinct("genres").length

// Requete 5 - Combien un réalisateur a-t-il tourné de films en moyenne ?
let total = db.movies.count()
let réalisateur = db.movies.distinct("director_name").length
let moyenne = total / réalisateur

// Requete 6 - De quelle couleur sont les films... qui ne sont pas en couleur ?
db.movies.find(
    { color: {$ne: "Color"}},
    { color: true, movie_title: true, _id: false}
)

db.movies.distinct("color", {color: {$ne: "Color"}})

// Requete 7 - Dans quels films intervient Clint Eastwood...

// - en tant qu’acteur OU réalisateur ?
db.movies.find(
    {$or : [{"actors.name": "Clint Eastwood"}, {"director_name": "Clint Eastwood"} ] },
    { movie_title: true, director_name: true, actors: true, _id: false}
)

// - en tant qu’acteur ET réalisateur ?
db.movies.find(
    { "actors.name": "Clint Eastwood", "director_name": "Clint Eastwood" },
    { movie_title: true, director_name: true, actors: true, _id: false}
)

// - en tant qu’acteur, SANS être réalisateur ?
db.movies.find(
    { "actors.name": "Clint Eastwood", "director_name": {$ne: "Clint Eastwood"} },
    { movie_title: true, director_name: true, actors: true, _id: false}
)

db.movies.find(
    { "actors.name": "Clint Eastwood", "director_name": /[^Clint Eastwood]/i },
    { movie_title: true, director_name: true, actors: true, _id: false}
)
